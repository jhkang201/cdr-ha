package com.sktelecom.bill.cdr_ha;

import java.util.List;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.BackgroundCallback;
import org.apache.curator.framework.api.CuratorEvent;
import org.apache.curator.framework.api.CuratorListener;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.utils.CloseableUtils;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.Watcher;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.ConfigurationException;

public class HAExecutor {
	public static void main(String[] args) {
		
		PropertiesConfiguration property = null;
		CuratorFramework client = null;
		String dirDelimeter = "";
		String agentName = "";
		if(args.length<1) {
			System.err.println("USAGE: HAExecutor agentName");
			System.err.println("ex> java HAExecutor LR");
			System.exit(-1);
		} else {
			agentName = args[0];
		}
		
		try {
			property = new PropertiesConfiguration("zkMonitor.properties");
			String runHost = property.getString("zkMonitor.host");
			String zkConnString = property.getString("zkMonitor.zookeeper.hosts", "127.0.0.1:2181");
			String dirBin = property.getString("zkMonitor.exec.dirBin");
			String dirConf = property.getString("zkMonitor.exec.dirConf");
			
			dirDelimeter = property.getString("zkMonitor.dirDelimeter");
			
			client = CuratorFrameworkFactory.newClient(zkConnString,
					new ExponentialBackoffRetry(1000, 3));
			client.start();
			
			String exec = dirBin+dirDelimeter+"flume-ng agent -n " +agentName
						+ " -c "+dirConf
						+ " -f "+dirConf+dirDelimeter+agentName+ ".properties";
			
			if(client.checkExists().forPath("/HaMonitor/CDR/"+agentName) == null) {
				try {
					createEphemeral(client, "/HaMonitor/CDR/"+agentName, runHost.getBytes());
					//create(client, "/HaMonitor/"+targetId, runHost.getBytes());
					System.out.println("\t[Zookeeper]  :/HaMonitor/CDR/"+agentName+" Register O.K!!");
					Process proc = Runtime.getRuntime().exec(exec);
					
					do {
						//System.out.println("\t["+System.currentTimeMillis()+"] "+new String(client.getData().forPath("/HaMonitor/CDR/"+agentName)));
						Thread.sleep(30*1000);
					} while(client.checkExists().forPath("/HaMonitor/CDR/"+agentName) != null);
				} catch(Exception e) {
					e.printStackTrace();
					System.err.println("\t[Zookeeper]  :/HaMonitor Error on registering");
				}
			} else {
				System.out.println("\t Already, registered program !!");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			CloseableUtils.closeQuietly(client);
		}
	}

	public static void create(CuratorFramework client, String path,
			byte[] payload) throws Exception {
		// this will create the given ZNode with the given data
		client.create().creatingParentsIfNeeded().forPath(path, payload);
	}

	public static void createEphemeral(CuratorFramework client, String path,
			byte[] payload) throws Exception {
		// this will create the given EPHEMERAL ZNode with the given data
		client.create().creatingParentsIfNeeded()
				.withMode(CreateMode.EPHEMERAL).forPath(path, payload);
	}

	public static String createEphemeralSequential(CuratorFramework client,
			String path, byte[] payload) throws Exception {
		// this will create the given EPHEMERAL-SEQUENTIAL ZNode with the given
		// data using Curator protection.
		return client.create().creatingParentsIfNeeded().withProtection()
				.withMode(CreateMode.EPHEMERAL_SEQUENTIAL)
				.forPath(path, payload);
	}

	public static void setData(CuratorFramework client, String path,
			byte[] payload) throws Exception {
		// set data for the given node
		client.setData().forPath(path, payload);
	}

	public static void setDataAsync(CuratorFramework client, String path,
			byte[] payload) throws Exception {
		// this is one method of getting event/async notifications
		CuratorListener listener = new CuratorListener() {
			//@Override
			public void eventReceived(CuratorFramework client,
					CuratorEvent event) throws Exception {
				System.out.println("setDataAsync: " + event);
			}
		};
		client.getCuratorListenable().addListener(listener);
		// set data for the given node asynchronously. The completion
		// notification
		// is done via the CuratorListener.
		client.setData().inBackground().forPath(path, payload);
	}

	public static void setDataAsyncWithCallback(CuratorFramework client,
			BackgroundCallback callback, String path, byte[] payload)
			throws Exception {
		// this is another method of getting notification of an async completion
		client.setData().inBackground(callback).forPath(path, payload);
	}

	public static void delete(CuratorFramework client, String path)
			throws Exception {
		// delete the given node
		client.delete().deletingChildrenIfNeeded().forPath(path);
	}

	public static void guaranteedDelete(CuratorFramework client, String path)
			throws Exception {
		// delete the given node and guarantee that it completes
		client.delete().guaranteed().forPath(path);
	}

	public static List<String> watchedGetChildren(CuratorFramework client,
			String path) throws Exception {
		/**
		 * Get children and set a watcher on the node. The watcher notification
		 * will come through the CuratorListener (see setDataAsync() above).
		 */
		return client.getChildren().watched().forPath(path);
	}

	public static List<String> watchedGetChildren(CuratorFramework client,
			String path, Watcher watcher) throws Exception {
		/**
		 * Get children and set the given watcher on the node.
		 */
		return client.getChildren().usingWatcher(watcher).forPath(path);
	}

}