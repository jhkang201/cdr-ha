#!/bin/bash

####################
## Java ClassPath
myProgram=com.sktelecom.bill.cdr_ha.HAExecutor
myPathBase=/data/jhk/cdr-ha
myPath=\
$myPathBase/zookeeper-3.4.6.jar:\
$myPathBase/curator-framework-2.7.1.jar:\
$myPathBase/commons-configuration-1.9.jar:\
$myPathBase/log4j-1.2.16.jar:\
$myPathBase/cdr-ha.jar:\
$myPathBase/junit-3.8.1.jar:\
$myPathBase/curator-recipes-2.7.1.jar:\
$myPathBase/curator-client-2.7.1.jar:\
$myPathBase/slf4j-api-1.6.1.jar:\
$myPathBase/jline-0.9.94.jar:\
$myPathBase/netty-3.7.0.Final.jar:\
$myPathBase/guava-16.0.1.jar:\
$myPathBase/commons-lang-2.6.jar:\
$myPathBase/commons-logging-1.1.1.jar

## Agent Lists
agentListCDR="BC BS CG CS CX GC GS HA HB IC LM LP MM MR NA NG OM OX P1_META PV RD SM SS UX WC WF WG WK WP"
agentListUDR="KR LR P1 R1 R2 RR"
agentList="$agentListCDR $agentListUDR"

## Flume Config Directory
if [ -z "$FLUME_HOME" ]
then
	echo "You must define the variable 'FLUME_HOME' which home directory of FLUME"
	exit -1
else 
	echo "FLUME_HOME=$FLUME_HOME"
fi
confDir=$FLUME_HOME/conf


#echo "--------------------------"
#echo $myPath

for agent in $agentList
do
	confFile="$confDir/$agent.properties"
	echo $confFile
	if [ -e $confFile ] && [ -s $confFile ]
	then
		echo "$agent is avaialble"
		#echo "java -classpath $myPath $myProgram $agent"
		java -classpath $myPath -DCDRFlumeMon=$agent $myProgram $agent &
	else
		echo "$agent has no config file"
	fi
done
